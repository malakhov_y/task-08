package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private final StringBuilder currentValue = new StringBuilder();

    Flower currentFlower;
    List<Flower> flowerList;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public void parseXML() throws ParserConfigurationException, SAXException, IOException {

        // obtain sax parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();

        // XML document contains namespaces
        factory.setNamespaceAware(true);

        factory.setFeature("http://xml.org/sax/features/validation", true);
        factory.setFeature("http://apache.org/xml/features/validation/schema", true);

        SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
        for (Flower flower : flowerList) {
            System.out.println(flower.toString());
        }
    }


    @Override
    public void startDocument() {
        flowerList = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        // reset the tag value
        currentValue.setLength(0);

        if (qName.equalsIgnoreCase("flower"))
            currentFlower = new Flower();

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            String measure = attributes.getValue("measure");
            currentFlower.setAveLenFlowerMeasure(measure);
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            String measure = attributes.getValue("measure");
            currentFlower.setTempretureMesure(measure);
        }

        if (qName.equalsIgnoreCase("lighting")) {
            String measure = attributes.getValue("lightRequiring");
            currentFlower.setLightRequiring(measure);
        }

        if (qName.equalsIgnoreCase("watering")) {
            String measure = attributes.getValue("measure");
            currentFlower.setWateringMeasure(measure);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {

        if (qName.equalsIgnoreCase("name"))
            currentFlower.setName(currentValue.toString());

        if (qName.equalsIgnoreCase("soil"))
            currentFlower.setSoil(currentValue.toString());

        if (qName.equalsIgnoreCase("origin"))
            currentFlower.setOrigin(currentValue.toString());

        if (qName.equalsIgnoreCase("stemColour"))
            currentFlower.setStemColour(currentValue.toString());

        if (qName.equalsIgnoreCase("leafColour"))
            currentFlower.setLeafColour(currentValue.toString());

        if (qName.equalsIgnoreCase("aveLenFlower"))
            currentFlower.setAveLenFlower(currentValue.toString());

        if (qName.equalsIgnoreCase("tempreture"))
            currentFlower.setTempreture(currentValue.toString());

        if (qName.equalsIgnoreCase("watering"))
            currentFlower.setWatering(currentValue.toString());

        if (qName.equalsIgnoreCase("multiplying")) {
            currentFlower.setMultiplying(currentValue.toString());
            flowerList.add(currentFlower);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) {
        currentValue.append(ch, start, length);
    }


}