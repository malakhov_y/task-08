package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.controller.entity.Flower;

import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        String outputXmlFile = "output.dom.xml";
        DOMController domController = new DOMController(xmlFileName);
        // PLACE YOUR CODE HERE
        domController.parseXML();
        domController.writeXML(outputXmlFile);

        // sort (case 1)
        // PLACE YOUR CODE HERE

        // save

        // PLACE YOUR CODE HERE

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////
        SAXController saxController = new SAXController(xmlFileName);
        outputXmlFile = "output.sax.xml";
        saxController.parseXML();
        List<Flower> flowerList = saxController.getFlowerList();
        domController.setFlowerList(flowerList);
        domController.writeXML(outputXmlFile);


        // get


		// PLACE YOUR CODE HERE

        // sort  (case 2)
        // PLACE YOUR CODE HERE

        // save

        // PLACE YOUR CODE HERE
//        domController.saveToXML(flowerList, outputXmlFile);
        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parseXML();
        outputXmlFile = "output.stax.xml";
        flowerList = saxController.getFlowerList();
        domController.setFlowerList(flowerList);
        domController.writeXML(outputXmlFile);
        // PLACE YOUR CODE HERE

        // sort  (case 3)
        // PLACE YOUR CODE HERE

        // save

        // PLACE YOUR CODE HERE
    }

}
