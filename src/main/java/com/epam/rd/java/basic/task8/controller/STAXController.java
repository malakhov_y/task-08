package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    List<Flower> flowerList = new ArrayList<>();

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parseXML() {
        Flower flower = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            flower = new Flower();
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "soil":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setSoil(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "stemColour":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setStemColour(xmlEvent.asCharacters().getData());
                            break;
                        case "leafColour":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setLeafColour(xmlEvent.asCharacters().getData());
                            break;
                        case "aveLenFlower": {
                            xmlEvent = xmlEventReader.nextEvent();
                            Attribute idAttr = startElement.getAttributeByName(new QName("measure"));
                            flower.setAveLenFlowerMeasure(idAttr.getValue());
                            flower.setAveLenFlower(xmlEvent.asCharacters().getData());
                            break;
                        }
                        case "tempreture": {
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setTempreture(xmlEvent.asCharacters().getData());
                            Attribute idAttr = startElement.getAttributeByName(new QName("measure"));
                            flower.setTempretureMesure(idAttr.getValue());
                            break;
                        }
                        case "lighting": {
                            Attribute idAttr = startElement.getAttributeByName(new QName("lightRequiring"));
                            flower.setLightRequiring(idAttr.getValue());
                            break;
                        }
                        case "watering": {
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setWatering(xmlEvent.asCharacters().getData());
                            Attribute idAttr = startElement.getAttributeByName(new QName("measure"));
                            flower.setWateringMeasure(idAttr.getValue());
                            break;
                        }
                        case "multiplying":
                            xmlEvent = xmlEventReader.nextEvent();
                            flower.setMultiplying(xmlEvent.asCharacters().getData());
                            break;
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        flowerList.add(flower);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        for (Flower flwr : flowerList) {
            System.out.println(flwr.toString());
        }
    }

    // PLACE YOUR CODE HERE

}