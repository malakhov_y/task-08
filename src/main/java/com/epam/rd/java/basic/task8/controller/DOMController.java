package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.controller.entity.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;

    List<Flower> flowerList = new ArrayList<>();

    public void setFlowerList(List<Flower> flowerList) {
        this.flowerList = flowerList;
    }

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parseXML() throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        docFactory.setFeature("http://xml.org/sax/features/validation", true);
        docFactory.setFeature("http://apache.org/xml/features/validation/schema", true);

        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(xmlFileName);
        doc.getDocumentElement().normalize();

        System.out.println("Корневой элемент: " + doc.getDocumentElement().getNodeName());
        NodeList nodeList = doc.getElementsByTagName("flower");

        for (int i = 0; i < nodeList.getLength(); i++) {
            flowerList.add(getFlower(nodeList.item(i)));
        }
        for (Flower flower : flowerList) {
            System.out.println(flower.toString());
        }
    }
    private Flower getFlower(Node node) {
        Flower flower = new Flower();
//        if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        flower.setName(getTagValue("name", element));
        flower.setOrigin(getTagValue("origin", element));
        flower.setMultiplying(getTagValue("multiplying", element));
        flower.setSoil(getTagValue("soil", element));
        flower.setStemColour(getTagValue("stemColour", element));
        flower.setLeafColour(getTagValue("leafColour", element));

        flower.setAveLenFlower(getTagValue("aveLenFlower", element));
        flower.setTempreture(getTagValue("tempreture", element));
        flower.setWatering(getTagValue("watering", element));

        flower.setLightRequiring(element.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getTextContent());
        flower.setAveLenFlowerMeasure(element.getElementsByTagName("aveLenFlower").item(0).getAttributes().getNamedItem("measure").getTextContent());
        flower.setTempretureMesure(element.getElementsByTagName("tempreture").item(0).getAttributes().getNamedItem("measure").getTextContent());
        flower.setWateringMeasure(element.getElementsByTagName("watering").item(0).getAttributes().getNamedItem("measure").getTextContent());
//        }
        return flower;
    }
    private String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    public void writeXML(String outputXmlFile) throws ParserConfigurationException, TransformerException, FileNotFoundException {

        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElementNS("http://www.nure.ua","flowers");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        doc.appendChild(rootElement);

        for (Flower value : flowerList) {
            Element flower = doc.createElement("flower");
            rootElement.appendChild(flower);

            Element name = doc.createElement("name");
            name.setTextContent(value.getName());
            flower.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(value.getSoil());
            flower.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(value.getOrigin());
            flower.appendChild(origin);

            // visualParameters
            Element visualParameters = doc.createElement("visualParameters");
            flower.appendChild(visualParameters);

            Element stemColour = doc.createElement("stemColour");
            stemColour.setTextContent(value.getStemColour());
            visualParameters.appendChild(stemColour);

            Element leafColour = doc.createElement("leafColour");
            leafColour.setTextContent(value.getLeafColour());
            visualParameters.appendChild(leafColour);

            Element aveLenFlower = doc.createElement("aveLenFlower");
            aveLenFlower.setTextContent(value.getAveLenFlower());
            aveLenFlower.setAttribute("measure", flowerList.get(0).getAveLenFlowerMeasure());
            visualParameters.appendChild(aveLenFlower);

            // growingTips
            Element growingTips = doc.createElement("growingTips");
            flower.appendChild(growingTips);

            Element tempreture = doc.createElement("tempreture");
            tempreture.setTextContent(value.getTempreture());
            tempreture.setAttribute("measure", flowerList.get(0).getTempretureMesure());
            growingTips.appendChild(tempreture);

            Element lighting = doc.createElement("lighting");
            lighting.setAttribute("lightRequiring", flowerList.get(0).getLightRequiring());
            growingTips.appendChild(lighting);

            Element watering = doc.createElement("watering");
            watering.setTextContent(value.getWatering());
            watering.setAttribute("measure", flowerList.get(0).getWateringMeasure());
            growingTips.appendChild(watering);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(value.getMultiplying());
            flower.appendChild(multiplying);
        }
        writeXml(doc, outputXmlFile);

    }

    public void writeXml(Document doc, String outputXmlFile) throws TransformerException, FileNotFoundException {


        FileOutputStream output = new FileOutputStream(outputXmlFile);



        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        doc.setXmlStandalone(true);

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }
}
