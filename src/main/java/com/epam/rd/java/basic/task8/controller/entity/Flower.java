package com.epam.rd.java.basic.task8.controller.entity;

public class Flower {
    String name;
    String soil;
    String origin;
    String stemColour;
    String leafColour;
    String aveLenFlower;
    String aveLenFlowerMeasure;
    String tempreture;
    String tempretureMesure;
    String lightRequiring;
    String watering;
    String wateringMeasure;
    String multiplying;

    public Flower() {
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setSoil(String soil) {
        this.soil = soil;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }
    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }
    public void setAveLenFlower(String aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }
    public void setTempreture(String tempreture) {
        this.tempreture = tempreture;
    }
    public void setTempretureMesure(String tempretureMesure) {
        this.tempretureMesure = tempretureMesure;
    }
    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }
    public void setWatering(String watering) {
        this.watering = watering;
    }
    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }
    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }
    public String getName() {
        return name;
    }
    public String getSoil() {
        return soil;
    }
    public String getOrigin() {
        return origin;
    }
    public String getStemColour() {
        return stemColour;
    }
    public String getLeafColour() {
        return leafColour;
    }
    public String getAveLenFlower() {
        return aveLenFlower;
    }
    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }
    public String getTempreture() {
        return tempreture;
    }
    public String getTempretureMesure() {
        return tempretureMesure;
    }
    public String getLightRequiring() {
        return lightRequiring;
    }
    public String getWatering() {
        return watering;
    }
    public String getWateringMeasure() {
        return wateringMeasure;
    }
    public String getMultiplying() {
        return multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                ", tempreture='" + tempreture + '\'' +
                ", tempretureMesure='" + tempretureMesure + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering='" + watering + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
